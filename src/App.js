import { useContext } from "react";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import AllProduct from "./components/allProduct/AllProduct";
import CatalogProduct from "./components/catalogProduct/CatalogProduct";
import EditForm from "./components/editForm/EditForm";
import Form from "./components/form/Form";
import Loading from "./components/loading/Loading";
import Login from "./components/login/Login";
import ProductTable from "./components/productTable/ProductTable";
import Register from "./components/register/Register";
import DisplayProduct from "./components/displayProduct/DisplayProduct";
import { GlobalContext } from "./context/GlobalContext";
import ProtectedRoute from "./inherit/ProtectedRoute";
import "antd/dist/antd.css";
import "./App.css";

const router = createBrowserRouter([
  {
    path: "/",
    element: <CatalogProduct />,
  },
  {
    path: "/products",
    element: <AllProduct />,
  },
  {
    path: "/displayProduct/:id",
    element: <DisplayProduct />,
  },
  {
    path: "/login",
    element: <Login />,
  },
  {
    path: "/register",
    element: <Register />,
  },
  {
    element: <ProtectedRoute />,
    children: [
      {
        path: "/manage",
        element: <ProductTable />,
      },
      {
        path: "/create",
        element: <Form />,
      },
      {
        path: "/edit/:id",
        element: <EditForm />,
      },
    ],
  },
]);

function App() {
  const { loading } = useContext(GlobalContext);
  return (
    <div className="App ant-layout min-h-screen">
      {loading && <Loading />}
      <RouterProvider router={router} />
    </div>
  );
}

export default App;
