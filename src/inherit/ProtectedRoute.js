import React from "react";
import { Navigate, Outlet } from "react-router-dom";

const ProtectedRoute = ({ children }) => {
  if (localStorage.getItem("token") === null) {
    return <Navigate to="/login" replace />;
  }
  return children ?? <Outlet />;
};

export default ProtectedRoute;
