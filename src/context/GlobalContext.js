import { createContext, useState } from "react";

export const GlobalContext = createContext();

export const GlobalProvider = ({ children }) => {
  const [products, setProducts] = useState([]);
  const [edit, setEdit] = useState({});
  const [loading, setLoading] = useState(false);
  return (
    <GlobalContext.Provider
      value={{ products, setProducts, edit, setEdit, loading, setLoading }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
