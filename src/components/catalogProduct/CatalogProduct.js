import axios from "axios";
import React, { useContext, useEffect } from "react";
import { GlobalContext } from "../../context/GlobalContext";
import CardContent from "../cardContent/CardContent";
import Navbar from "../loading/Navbar";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { Carousel } from "antd";
import "antd/dist/antd.css";

const MySwal = withReactContent(Swal);

function CatalogProduct() {
  const { products, setProducts, setLoading } = useContext(GlobalContext);

  const fetchProducts = async () => {
    try {
      setLoading(true);
      const response = await axios.get(
        "https://arhandev.xyz/public/api/final/products/home"
      );
      setProducts(response.data.data);
      setLoading(false);
    } catch (e) {
      console.log(e);
      MySwal.fire({
        title: <strong>Error</strong>,
        html: "Oops, Something Went Wrong!",
        icon: "error",
      });
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchProducts();
  }, []);

  const contentStyle = {
    height: "160px",
    color: "#fff",
    lineHeight: "160px",
    textAlign: "center",
    background: "#364d79",
  };

  return (
    <section>
      <Navbar />
      <Carousel autoplay>
        <div>
          <h3 style={contentStyle}>Welcome Back</h3>
        </div>
        <div>
          <h3 style={contentStyle}>Dear</h3>
        </div>
        <div>
          <h3 style={contentStyle}>{localStorage.getItem('username')}</h3>
        </div>
      </Carousel>
      <div className="w-10/12 mx-auto">
        <div className="flex justify-between items-center">
          <h1 className="text-3xl mt-10 mb-8 font-bold">Catalog Product</h1>
          <a href="/products">
            <button className="text-white bg-cyan-700 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-xl font-medium">
              See More
            </button>
          </a>
        </div>
        <div className="grid grid-cols-4 gap-10">
          {products.length === 0 ? (
            <h3>Tidak ada product</h3>
          ) : (
            products.map((item, index) => (
              <CardContent key={index} data={item} />
            ))
          )}
        </div>
      </div>
    </section>
  );
}

export default CatalogProduct;
