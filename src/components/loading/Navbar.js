import axios from "axios";
import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { Disclosure } from "@headlessui/react";
import "../../App.css";

function Navbar() {
  const navigate = useNavigate();

  const onLogout = async () => {
    try {
      const response = await axios.post(
        "https://arhandev.xyz/public/api/final/logout",
        {},
        {
          headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
        }
      );
      localStorage.removeItem("token");
      localStorage.removeItem("username");
      navigate("/");
    } catch (error) {
      alert(error.response.data.info);
    }
  };
  return (
    <Disclosure as="nav" className="bg-gray-800">
      <div className="relative flex h-16 items-center justify-between padding-nav">
        <div className="flex flex-1 items-center justify-center sm:justify-start">
          <div className="flex flex-shrink-0 items-center">
            <img
              className="block h-8 w-auto lg:hidden"
              src="https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg"
              alt="Your Company"
            />
            <img
              className="hidden h-8 w-auto lg:block"
              src="https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg"
              alt="Your Company"
            />
          </div>
          <div className="hidden sm:ml-6 sm:block">
            <div className="flex space-x-4">
              <Link to="/">
                <div className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-xl font-medium">
                  Home
                </div>
              </Link>
              <Link to="/products">
                <div className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-xl font-medium">
                  All Products
                </div>
              </Link>
              <Link to="/manage">
                <div className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-xl font-medium">
                  Manage Products
                </div>
              </Link>
            </div>
          </div>
        </div>
        {localStorage.getItem("token") === null ? (
          <nav className="flex gap-4">
            <Link to="/login">
              <div className="px-6 bg-sky-900 text-white rounded-lg font-bold py-1 hover:bg-gray-700 hover:text-white">
                Login
              </div>
            </Link>
            <Link to="/register">
              <div className="px-6 bg-white text-sky-900 rounded-lg font-bold py-1 hover:bg-gray-700 hover:text-white">
                Register
              </div>
            </Link>
          </nav>
        ) : (
          <nav className="flex gap-4 items-center">
            <div className="font-bold text-white">
              Hi, {localStorage.getItem("username")}
            </div>
            <button
              onClick={onLogout}
              className="px-6 py-1 bg-red-800 text-white bg-red rounded-lg hover:bg-gray-700 hover:text-white font-bold"
            >
              Logout
            </button>
          </nav>
        )}
      </div>
    </Disclosure>
  );
}

export default Navbar;
