import axios from "axios";
import React, { useContext, useEffect } from "react";
import { GlobalContext } from "../../context/GlobalContext";
import Card from "../cardContent/CardContent";
import Navbar from "../loading/Navbar";
import "./allProducts.css"

function AllProduct() {
  const { products, setProducts, setLoading } = useContext(GlobalContext);

  const fetchProducts = async () => {
    try {
      setLoading(true);
      const response = await axios.get(
        "https://arhandev.xyz/public/api/final/products"
      );
      setProducts(response.data.data);
      setLoading(false);
    } catch (e) {
      console.log(e);
      alert("terjadi suatu error");
      setLoading(false);
    }
  };
  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <div className="div-color div-margin">
      <Navbar />
      <div className="w-10/12 mx-auto div-margin">
        <h1 className="text-3xl mt-10 mb-8 font-bold">All Products</h1>
        <div className="grid grid-cols-4 gap-10">
          {products.length === 0 ? (
            <h3>Tidak ada product</h3>
          ) : (
            products.map((item, index) => <Card key={index} data={item} />)
          )}
        </div>
      </div>
    </div>
  );
}

export default AllProduct;