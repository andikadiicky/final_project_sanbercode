import axios from "axios";
import { Formik } from "formik";
import React from "react";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";
import Navbar from "../loading/Navbar";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

const validationSchema = Yup.object({
  email: Yup.string().required("Email wajib diisi").email("Email tidak valid"),
  name: Yup.string().required("Nama wajib diisi"),
  username: Yup.string().required("Username wajib diisi"),
  password: Yup.string().required("Password wajib diisi"),
  password_confirmation: Yup.string().required(
    "Konfirmasi Password wajib diisi"
  ),
});

const MySwal = withReactContent(Swal);

function Register() {
  const initialState = {
    email: "",
    name: "",
    username: "",
    password: "",
    password_confirmation: "",
  };
  const navigate = useNavigate();
  const onSubmit = async (values) => {
    try {
      const response = await axios.post(
        "https://arhandev.xyz/public/api/final/register",
        {
          email: values.email,
          username: values.username,
          name: values.email,
          password: values.password,
          password_confirmation: values.password_confirmation,
        }
      );

      MySwal.fire({
        title: <strong>Success!</strong>,
        html: "Account Successfully Created",
        icon: "success",
      });
      navigate("/login");
    } catch (error) {
      MySwal.fire({
        title: <strong>Error</strong>,
        html: error.response.data.info,
        icon: "error",
      });
    }
  };
  return (
    <div>
      <Navbar />
      <div className="max-w-2xl mx-auto border border-sky-900 rounded-lg p-8 mt-16 bg-white">
        <h1 className="text-center mb-6 text-2xl font-bold">Register Form</h1>
        <Formik
          onSubmit={onSubmit}
          initialValues={initialState}
          validationSchema={validationSchema}
        >
          {({
            handleSubmit,
            handleBlur,
            handleChange,
            values,
            errors,
            touched,
          }) => (
            <form
              onSubmit={handleSubmit}
              className="text-lg flex flex-col gap-2 items-center"
            >
              <div className="w-full grid grid-cols-3">
                <label>Email:</label>
                <input
                  className="col-span-2 border border-gray-400 py-0.5 px-2"
                  onChange={handleChange}
                  value={values.email}
                  onBlur={handleBlur}
                  type="text"
                  name="email"
                />
                <div></div>
                <div className="col-span-2 text-red-400 mb-3 text-base">
                  {touched.email && errors.email}
                </div>
              </div>
              <div className="w-full grid grid-cols-3">
                <label>Nama:</label>
                <input
                  className="col-span-2 border border-gray-400 py-0.5 px-2"
                  onChange={handleChange}
                  value={values.name}
                  onBlur={handleBlur}
                  type="text"
                  name="name"
                />
                <div></div>
                <div className="col-span-2 text-red-400 mb-3 text-base">
                  {touched.name && errors.name}
                </div>
              </div>
              <div className="w-full grid grid-cols-3">
                <label>Username:</label>
                <input
                  className="col-span-2 border border-gray-400 py-0.5 px-2"
                  onChange={handleChange}
                  value={values.username}
                  onBlur={handleBlur}
                  type="text"
                  name="username"
                />
                <div></div>
                <div className="col-span-2 text-red-400 mb-3 text-base">
                  {touched.username && errors.username}
                </div>
              </div>
              <div className="w-full grid grid-cols-3">
                <label>Password:</label>
                <input
                  className="col-span-2 border border-gray-400 py-0.5 px-2"
                  type="password"
                  onChange={handleChange}
                  value={values.password}
                  onBlur={handleBlur}
                  name="password"
                />
                <div></div>
                <div className="col-span-2 text-red-400 mb-3 text-base">
                  {touched.password && errors.password}
                </div>
              </div>
              <div className="w-full grid grid-cols-3">
                <label>Konfirmasi Password:</label>
                <input
                  className="col-span-2 border border-gray-400 py-0.5 px-2"
                  type="password"
                  onChange={handleChange}
                  value={values.password_confirmation}
                  onBlur={handleBlur}
                  name="password_confirmation"
                />
                <div></div>
                <div className="col-span-2 text-red-400 mb-3 text-base">
                  {touched.password_confirmation &&
                    errors.password_confirmation}
                </div>
              </div>
              <button
                className="text-white bg-cyan-700 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-lg font-bold"
                type="submit"
              >
                Register
              </button>
            </form>
          )}
        </Formik>
      </div>
    </div>
  );
}

export default Register;
