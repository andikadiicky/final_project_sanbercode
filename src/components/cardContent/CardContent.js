import React from "react";
import "antd/dist/antd.css";

function CardContent({ data }) {
  return (
    <div>
      <a href={`/displayProduct/${data.id}`}>
        <div className="ant-card ant-card-bordered ant-card-hoverable overflow-hidden rounded-xl">
          <div className="ant-card-cover">
            <img
              src={data.image_url}
              className="w-full h-72 object-cover"
              alt="example"
            />
          </div>

          <div className="ant-card-body">
            <div className="ant-card-meta">
              <div className="ant-card-meta-detail">
                <div className="ant-card-meta-description">
                  <div className="flex flex-col justify-between">
                    <h1 className="text-2xl font-bold truncate">{data.nama}</h1>
                    {data.is_diskon === 1 ? (
                      <div>
                        <p className="text-gray-700 text-base line-through">
                          Rp {data.harga_display}
                        </p>
                        <p className="text-xl text-red-500 font-bold">
                          Rp {data.harga_diskon_display}
                        </p>
                      </div>
                    ) : (
                      <p className="text-xl text-blue-800 font-bold">
                        Rp {data.harga_display}
                      </p>
                    )}
                    <div className="pt-4 pb-2">
                      <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">
                        Stock {data.stock}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>
  );
}

export default CardContent;
