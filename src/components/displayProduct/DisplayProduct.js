import React from 'react'
import axios from "axios";
import { useEffect, useState, useContext } from "react";
import Navbar from "../loading/Navbar";
import { GlobalContext } from "../../context/GlobalContext";
import { useParams } from "react-router-dom";

function DisplayProduct() {
  const { setLoading } = useContext(GlobalContext);
  const { id } = useParams();

  const [input, setInput] = useState({
    nama: "",
    harga_display: 0,
    harga_diskon_display: 0,
    is_diskon: false,
    image_url: "",
    description: "",
    category: "",
    stock: 0,
  });

  const fetchProduct = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        `https://arhandev.xyz/public/api/final/products/${id}`
      );
      setInput({
        nama: response.data.data.nama,
        harga_display: response.data.data.harga_display,
        harga_diskon_display: response.data.data.harga_diskon_display,
        is_diskon: response.data.data.is_diskon,
        image_url: response.data.data.image_url,
        description: response.data.data.description, 
        category: response.data.data.category,
        stock: response.data.data.stock,
      });
      console.log(response);
    } catch (e) {
      console.log(e);
      alert(e.response.data.info);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchProduct();
  }, []);

  return (
    <div>
      <Navbar />
        <div className="w-10/12 mx-auto mt-24">
            <div className="grid grid-cols-12 gap-24 p-12  bg-white rounded-md">
                <div className="col-span-5">
                    <img className="w-full object-cover" alt="" src={input.image_url} />
                </div>
                <div className="col-span-7 flex flex-col justify-around">
                    <h1 className="text-4xl font-bold">{input.nama}</h1>
                    <div>
                    {input.is_diskon === 1 ? (
                      <div>
                        <p className="text-xl text-gray-700 text-base line-through">
                          Rp {input.harga_display}
                        </p>
                        <p className="text-xl text-red-500 font-bold">
                          Rp {input.harga_diskon_display}
                        </p>
                      </div>
                    ) : (
                      <p className="text-xl text-blue-800 font-bold">
                        Rp {input.harga_display}
                      </p>
                    )}
                    </div>
                    <div className="text-xl"><span className="font-bold">Kategori: </span>{input.category}</div>
                    <div className="text-xl"><span className="font-bold">Deskripsi: </span>{input.description}</div>
                    <div className="pt-4 pb-2">
                      <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-xl font-semibold text-gray-700 mr-2 mb-2">
                        Stock {input.stock}
                      </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default DisplayProduct