import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { GlobalContext } from "../../context/GlobalContext";
import Navbar from "../loading/Navbar";
import "antd/dist/antd.css";
import { Table, Input, Select, Form } from "antd";
import "./productTable.css";

function ProductTable() {
  const { products, setEdit, setProducts, setLoading, loading } =
    useContext(GlobalContext);
  const { Search } = Input;
  const { Option } = Select;
  const [displayData, setDisplayData] = useState([]);
  const [search, setSearch] = useState("");
  const [filterCategory, setFilterCategory] = useState("");

  const onDelete = async (value) => {
    try {
      const response = await axios.delete(
        `https://arhandev.xyz/public/api/final/products/${value}`,
        {
          headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
        }
      );
      fetchProducts();
    } catch (e) {
      console.log(e);
      alert(e.response.data.info);
    }
  };

  const fetchProducts = async () => {
    try {
      setLoading(true);
      const response = await axios.get(
        "https://arhandev.xyz/public/api/final/products",
        {
          headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
        }
      );
      setProducts(response.data.data);
      setDisplayData(response.data.data);
      setLoading(false);
    } catch (e) {
      console.log(e);
      alert("terjadi suatu error");
      setLoading(false);
    }
  };

  const columns = [
    {
      title: "Product Name",
      dataIndex: "nama",
      key: "nama",
      align: "center",
    },
    {
      title: "Price",
      dataIndex: "harga",
      key: "harga",
      align: "center",
      render: (value, record) => (
        <div className="justify-center">Rp. {record.harga_display}</div>
      ),
    },
    {
      title: "Discount Price",
      dataIndex: "harga_diskon",
      key: "harga_diskon",
      align: "center",
      render: (value, record) => (
        <div className="justify-center">
          {record.is_diskon === 0 ? "-" : `Rp. ${record.harga_diskon_display}`}
        </div>
      ),
    },
    {
      title: "Image",
      dataIndex: "image_url",
      key: "image_url",
      align: "center",
      render: (value, record, index) => {
        // console.log(value, record, index);
        return (
          <img
            src={value}
            alt=""
            className="w-32 h-32 object-cover justify-center"
          ></img>
        );
      },
    },
    {
      title: "Stock",
      dataIndex: "stock",
      key: "stock",
      align: "center",
    },
    {
      title: "Category",
      dataIndex: "category",
      key: "category",
      align: "center",
    },
    {
      title: "Created By",
      dataIndex: "user",
      key: "user",
      align: "center",
      render: (value, record) => (
        <div className="justify-center">{record.user.email}</div>
      ),
    },
    {
      title: "Created At",
      dataIndex: "created_at",
      key: "created_at",
      align: "center",
    },
    {
      title: "Action",
      key: "action",
      align: "center",
      render: (value) => (
        <div className="flex gap-2 justify-center">
          <a href={`/edit/${value.id}`}>
            <button className="bg-amber-500 py-2 px-10 rounded-lg text-white font-bold hover:bg-gray-700 hover:text-white">
              Edit
            </button>
          </a>
          <button
            onClick={() => {
              onDelete(value.id);
            }}
            className="bg-red-600 py-2 px-10 rounded-lg text-white font-bold hover:bg-gray-700 hover:text-white"
          >
            Delete
          </button>
        </div>
      ),
    },
  ];

  const handleChange = (e) => {
    setSearch(e.target.value);
  };

  const handleChangeSelect = (value) => {
    setFilterCategory(value);
    let arrFilter = products.filter((item) => item.category.includes(value));
    console.log(arrFilter);
    setDisplayData(arrFilter);
  };

  const onSearch = () => {
    let arrSearch = products.filter((item) =>
      item.nama.toLowerCase().includes(search.toLowerCase())
    );
    setDisplayData(arrSearch);
  };

  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <div className="div-color">
      <Navbar />

      <div className="w-10/12 mx-auto flex justify-between items-center">
        <h1 className="text-3xl mt-10 mb-8 font-bold">Manage Products</h1>
        <Link to="/create">
          <button className="text-white bg-cyan-700 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-xl font-medium flex items-center gap-3">
            <span className="text-2xl">+</span> <span>Add Product</span>
          </button>
        </Link>
      </div>

      <div className="w-10/12 mx-auto flex justify-end items-center gap-6">
        <Select
          style={{ width: 180 }}
          value={filterCategory}
          onChange={handleChangeSelect}
        >
          <Option value="" disabled>
            Select an Option
          </Option>
          <Option value="teknologi">Technology</Option>
          <Option value="makanan">Food</Option>
          <Option value="minuman">Drink</Option>
          <Option value="lainnya">Others</Option>
        </Select>

        <Search
          placeholder="search products"
          style={{ width: 180 }}
          name="search"
          onChange={handleChange}
          onSearch={onSearch}
          value={search}
        />
      </div>

      <Table
        rowKey={"id"}
        columns={columns}
        dataSource={displayData}
        className="my-12 w-10/12 mx-auto mt-5 table-auto"
      />
    </div>
  );
}

export default ProductTable;
